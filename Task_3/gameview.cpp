#include "gameview.h"
#include <iostream>

using namespace std;

void ConsoleView::print_board_of_ship(short map[M][M], string name) {
	clear_screen();
	char c = 'A';
	cout << name << endl;
	cout << "Board of ships:\n" << endl;
	cout << "  0 1 2 3 4 5 6 7 8 9" << endl;
	for (int i = 0; i < M; i++) {
		cout << c << " ";
		for (int j = 0; j < M; j++) {
			switch (map[i][j]) {
			case 0: cout << "  "; break;
			case 1: cout << "- "; break;
			case 2: cout << "O "; break;
			}
		}
		cout << "\n";
		c++;
	}
}

void ConsoleView::print_board_of_strike(short map[M][M] , string name) {
	char c = 'A';
	cout << name << endl;
	cout << "Board of strike:\n" << endl;
	cout << "  0 1 2 3 4 5 6 7 8 9" << endl;
	for (int i = 0; i < M; i++) {
		cout << c << " ";
		for (int j = 0; j < M; j++) {
			switch (map[i][j]) {
			case 0: cout << "  "; break;
			case 1: cout << "X "; break;
			case 2: cout << "- "; break;
			}
		}
		cout << "\n";
		c++;
	}
}

void ConsoleView::print_winner_of_round(string name, int score_1, int score_2, int current_round) {
	clear_screen();
	cout << "In round #" << current_round << " " << name << " won with score: " << score_1 << " : " << score_2 << "." << endl;
}

void ConsoleView::print_statistic(map<int, string> &map) {
	clear_screen();
	for (auto it = map.begin(); it != map.end(); it++)
		cout << "In " << it->first << " round won " << it->second << endl;
}

void ConsoleView::print_current_round(int current_round) {
	clear_screen();
	cout << "Round #" << current_round << endl;
}

void ConsoleView::print_winner(string name_1, string name_2, int point_1, int point_2) {
	string winner = (point_1 > point_2) ? name_1 : name_2;
	int winner_points = (point_1 > point_2) ? point_1 : point_2;
	int loser_points = (point_1 > point_2) ? point_2 : point_1;
	cout << "Winner - " << winner << "!\nScore: " << winner_points << " : " << loser_points << endl;
	system("pause");
}

void ConsoleView::print_enter_name(int num){
	clear_screen();
	cout << "Player #" << num << ", enter your name: " << endl;
}

void ConsoleView::print_enter_coordinates(int count_of_deck, string name) {
	cout << name << ", enter coordinates and orientation (0 - horizontal, 1 - vertical)\nof " << count_of_deck << "-deck ship:" << endl;
}

void ConsoleView::clear_screen() {
	system("cls");
}

void ConsoleView::print_scores(string name_1, string name_2, int score_1, int score_2) {
	clear_screen();
	cout << name_1 << " scores: " << score_1 << endl;
	cout << name_2 << " scores: " << score_2 << endl;
}

void ConsoleView::print_destroy_ship() {
	cout << "The ship was destroyed!" << endl;
	system("pause");
}

void ConsoleView::print_deck_shot_down() {
	cout << "The ship's deck is shot down." << endl;
	system("pause");
}

void ConsoleView::print_exception(const char *str) {
	cout << str << endl;
	system("pause");
}

void ConsoleView::print_coord_for_strike(string name) {
	cout << name << ", enter coordinates for strike:" << endl;
}

void ConsoleView::print_didnt_hit() {
	cout << "You didn't hit." << endl;
	system("pause");
}

