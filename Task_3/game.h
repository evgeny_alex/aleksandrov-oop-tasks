#pragma once
#include "arbitr.h"

class GameLogic {
	private:
		Arbitr arbitr;
		shared_ptr<Player> player_1;
		shared_ptr<Player> player_2;
		int count_of_rounds;			// ���������� �������
		int current_round;
		void set_name();
		void set_ships();
		void game_cycle();
		void swap(shared_ptr<Player>*, shared_ptr<Player>*);
		GameView* gameview;
		map<int, string> map_statistic;
	public:
		GameLogic(shared_ptr<Player> player_1, shared_ptr<Player> player_2, int count); // ���������� ������ GameLogic
		void start();
};