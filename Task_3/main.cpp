#include "game.h"
#include <stdlib.h>
#include <iostream>
#include "anyoption.h"

void init_option(AnyOption *opt, int argc, char** argv) {
	opt->addUsage("Usage: ");
	opt->addUsage("");
	opt->addUsage(" -h --help  		Prints this help ");
	opt->addUsage(" -c --count		Default: 1, count of rounds in game");
	opt->addUsage(" -f --first		Default: random, type of first player");
	opt->addUsage(" -s --second		Default: random, type of second player ");

	opt->setFlag("help", 'h'); // ��������� � ������ � ��������
	opt->setOption("count", 'c');
	opt->setOption("first", 'f');
	opt->setOption("second", 's');

	opt->processCommandArgs(argc, argv); // ���������� �����
}

int main(int argc, char* argv[]) {
	int count_of_rounds = 1;

	shared_ptr<Player> player_1(new RandomPlayer);
	shared_ptr<Player> player_2(new RandomPlayer);

	AnyOption *opt = new AnyOption();

	init_option(opt, argc, argv);

	if (!opt->hasOptions()) { // ���� ��� �����
		opt->printUsage();
		delete opt;
		system("pause");
		return 0;
	}

	if (opt->getFlag("help") || opt->getFlag('h'))
		opt->printUsage();
	if (opt->getValue('c') != NULL || opt->getValue("count") != NULL)
		count_of_rounds = atoi(opt->getValue('c'));

	if (opt->getValue("first") != NULL || opt->getValue("f") != NULL) {
		string str = opt->getValue('f');
		if (str == "console") player_1 = make_shared<ConsolePlayer>();
		if (str == "optimal") player_1 = make_shared<OptimalPlayer>();
	}

	if (opt->getValue("second") != NULL || opt->getValue("s") != NULL) {
		string str = opt->getValue('s');
		if (str == "console") player_2 = make_shared<ConsolePlayer>();
		if (str == "optimal") player_2 = make_shared<OptimalPlayer>();
	}
	
	srand((unsigned int)time(0)); // ������� ��� ������ �������� �������

	GameLogic game(player_1, player_2, count_of_rounds);
 	game.start();

	delete opt;
	return 0;
}