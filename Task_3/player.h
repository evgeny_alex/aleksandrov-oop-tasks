#pragma once
#include <ctime>
#include "ship.h"
#include "exception.h"
#include "gameview.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <memory>
constexpr auto TEN = 10;

using namespace std;

const int N = 10;
enum count_of_ship {four_deck = 1, three_deck, two_deck, one_deck}; // ������������ ���������� ��������
enum State {find_deck, find_next_deck, finish_ship};

class Player {
protected:
	string name;
	GameView* gameview;
	bool flag_view;

public:
	virtual void enter_name() = 0;	//	���� �����

	// ����� ������
	virtual pair<int, int> generate_coord(count_of_deck, int) = 0;
	virtual int generate_direction() = 0;
	virtual pair<int, int> generate_strike() = 0;
	virtual void clear_next_moves() = 0;
	virtual void destroy_ship(int, int) = 0;
	virtual void new_strategy() = 0;
	virtual void deck_shot_down(int, int) = 0;

	bool get_flag_view();
	string get_name();
};

class ConsolePlayer : public Player {
	public:
		
		ConsolePlayer();				//	����������� ������ ConsolePlayer
		pair<int, int> generate_coord(count_of_deck, int);
		int generate_direction();
		pair<int, int> generate_strike();
		void clear_next_moves() {}
		void destroy_ship(int, int);
		void new_strategy() {}
		void deck_shot_down(int, int);

		void enter_name();					
};

class RandomPlayer : public Player {
	public:
		RandomPlayer();					// ���������� ������ RandomPlayer

		pair<int, int> generate_coord(count_of_deck, int);
		int generate_direction();
		pair<int, int> generate_strike();
		void clear_next_moves() {}
		void destroy_ship(int, int) {}
		void new_strategy() {}
		void deck_shot_down(int, int) {}

		void enter_name();
};

class OptimalPlayer : public Player {
private:
	State state;					// ��������� ������ ��������
	int count_found_4_deck_ship, count_found_3_deck_ship, count_found_2_deck_ship;
	int current_move;
	int count_found_deck;
	Direction dir_of_ships;

	int i, j; // �������, �� ������� �����, ����� ���� ��� ���������

	vector<short> x; // ������� ��������� ��������
	vector<short> y;

	vector<short> X_4_deck, Y_4_deck;	// ������� ��� �������� ���������������� ����� ������ ��������
	vector<short> X_3_deck, Y_3_deck;
	vector<short> X_2_deck, Y_2_deck;

	vector<short> X_next_moves, Y_next_moves;	// ����� ���������� ���� ��� ���������
	vector<short> X_ship, Y_ship;				// ���������� ����������� �������


	void init_strategy();
	void init_4_deck_ship();
	void init_3_deck_ship();
	void init_2_deck_ship();

	short min(vector<short> &);
	short max(vector<short> &);
	void init_coordinates(vector<short> &, vector<short> &);
	void swap(short*, short*);
	void init_big_ships();

public:
	OptimalPlayer();

	pair<int, int> generate_coord(count_of_deck, int);
	int generate_direction();
	pair<int, int> generate_strike();
	void clear_next_moves();
	void destroy_ship(int, int);
	void new_strategy();
	void deck_shot_down(int, int);

	void enter_name();										//	���� �����
};