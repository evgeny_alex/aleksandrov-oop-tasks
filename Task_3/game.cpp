#include "game.h"
#include <stdlib.h>

GameLogic::GameLogic(shared_ptr<Player> player_1, shared_ptr<Player> player_2, int count) : player_1(player_1), player_2(player_2),
																							count_of_rounds(count), gameview(new ConsoleView),
																							current_round(1) {}

void GameLogic::start() {
	set_name();
	for (int i = 1; i <= count_of_rounds; i++) {
		current_round = i;
		gameview->print_current_round(i);
		set_ships();
		game_cycle();
	}
	gameview->print_statistic(map_statistic);
	gameview->print_winner(player_1->get_name(), player_2->get_name(), arbitr.get_points(first), arbitr.get_points(second));
}

void GameLogic::set_name() { // ���� ���� �������
	gameview->print_enter_name(1);
	player_1->enter_name(); // �������� ����� ������
	gameview->print_enter_name(2);
	player_2->enter_name();
}

void GameLogic::set_ships() { // ��������� �������� �������
	arbitr.set_current_player(first);
	arbitr.placement_ship(player_1);
	arbitr.set_current_player(second);
	arbitr.placement_ship(player_2);
}

void GameLogic::game_cycle() {
	arbitr.set_current_player(first);
	shared_ptr<Player> cur_player = player_1;								// ������� �����
	shared_ptr<Player> opponent_player = player_2;
	
	while (!(arbitr.get_flag_player(first) || arbitr.get_flag_player(second))) {
		gameview->print_scores(player_1->get_name(), player_2->get_name(), arbitr.get_scores(first), arbitr.get_scores(second));	// ������ �����
		arbitr.print_map_strike(player_1->get_name(), first); // ������ ���� ������
		arbitr.print_map_strike(player_2->get_name(), second);

		if (arbitr.check_move(cur_player)) continue;					// ���� ������� ����� �����, �� ���������� ������
		swap(&cur_player, &opponent_player);
	}
	int cur = arbitr.get_current_player();
	arbitr.add_points(cur);		// ��������� ���� ����������� � ������ ������

	string score_1, score_2;
	score_1 = to_string(arbitr.get_scores(cur));
	score_2 = to_string(arbitr.get_scores((cur + 1)%2));

	map_statistic[current_round] = cur_player->get_name() + " with score: " + score_1 + " : " + score_2; // ���������� ������ � ����������
	gameview->print_winner_of_round(cur_player->get_name(), arbitr.get_scores(cur), arbitr.get_scores((cur + 1) % 2), current_round);

	arbitr.erase_data(); // ������� ������ ��� ������ ������
}

void GameLogic::swap(shared_ptr<Player>* current_player, shared_ptr<Player>* opponent_player) {
	shared_ptr<Player> p(new RandomPlayer);
	CurrentPlayer c = arbitr.get_current_player();
	if (c == first) arbitr.set_current_player(second);
	else arbitr.set_current_player(first);
	p = move(*current_player);
	*current_player = move(*opponent_player);
	*opponent_player = move(p);
}
