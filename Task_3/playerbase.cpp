#include "playerbase.h"

bool PlayerBase::is_this_ship(Ship &ship, short X, short Y) { // ����� ����������� � ���� �� ������� ����� ����
	short dX = ship.getX(), dY = ship.getY();
	if (ship.getDir() == h) dY += ship.getDeck() - 1;
	else dX += ship.getDeck() - 1;

	for (short i = ship.getX(); i <= dX; i++) {
		for (short j = ship.getY(); j <= dY; j++) {
			if (i == X && j == Y) {
				ship.reduce_deck();
				return true;
			}
		}
	}
	return false;
}

PlayerBase::PlayerBase(): defeat_flag(false), count_of_ships(TEN), scores(0), points(0), gameview(new ConsoleView) {
	for (int i = 0; i <= N; i++) {
		for (int j = 0; j <= N; j++) {
			map_ship[i][j] = 0;
			map_strike[i][j] = 0;
		}
	}
}

void PlayerBase::print_map_ship(string name) {
	gameview->print_board_of_ship(map_ship, name);
}

void PlayerBase::erase_data() {
	scores = 0;
	count_of_ships = N;
	defeat_flag = false;
	set_of_ships.clear();

	for (int i = 0; i <= N; i++) {
		for (int j = 0; j <= N; j++) {
			map_ship[i][j] = 0;
			map_strike[i][j] = 0;
		}
	}
}

int PlayerBase::get_points() {
	return points;
}

void PlayerBase::add_points() {
	points++;
}

void PlayerBase::set_ship_on_map_strike(Ship ship) {
	int x1, x2, y1, y2; // ������� ���������� ������� �������
	x1 = ship.getX() - 1;
	y1 = ship.getY() - 1;
	int dX = ship.getX(), dY = ship.getY();
	if (ship.getDir() == h) {
		dY = dY + ship.getDeck() - 1;
		x2 = ship.getX() + 1;
		y2 = ship.getY() + ship.getDeck();
	}
	else {
		dX = dX + ship.getDeck() - 1;
		x2 = ship.getX() + ship.getDeck();
		y2 = ship.getY() + 1;
	}
	// ��������� ������� ���������, ����� ������ 2 ��� �������
	if (x1 < 0) x1++;
	if (y1 < 0) y1++;
	if (x2 > 9) x2--;
	if (y2 > 9) y2--;
	for (int i = x1; i <= x2; i++) {
		for (int j = y1; j <= y2; j++) {
			if (i == N || j == N || map_strike[i][j] == 1) continue;
			map_strike[i][j] = 2;
		}
	}

	for (int i = ship.getX(); i <= dX; i++)
		for (int j = ship.getY(); j <= dY; j++)
			map_strike[i][j] = 1;
}

void PlayerBase::print_map_strike(string name) {
	gameview->print_board_of_strike(map_strike, name);
}

Ship PlayerBase::get_ship(int X, int Y) {
	size_t i;
	for (i = 0; i < set_of_ships.size() - 1; i++) {
		if (is_this_ship(set_of_ships[i], X, Y)) return set_of_ships[i];
	}
	return set_of_ships[i];
}

void PlayerBase::lost() {
	defeat_flag = true;
}

int PlayerBase::get_scores() {
	return scores;
}

bool PlayerBase::get_flag() {
	return defeat_flag;
}

int PlayerBase::get_count_ships() {
	return count_of_ships;
}

short PlayerBase::get_val_map_ship(int X, int Y) {
	return map_ship[X][Y];
}

short PlayerBase::get_val_map_strike(int X, int Y) {
	return map_strike[X][Y];
}

void PlayerBase::set_val_map_strike(int X, int Y, short a) {
	map_strike[X][Y] = a;
}

bool PlayerBase::impact_on_ships(short X, short Y) {
	if (map_ship[X][Y] == 2) return true;
	return false;
}

bool PlayerBase::check_ship(short X, short Y) { // �������� �������� �������
	for (size_t i = 0; i < set_of_ships.size(); i++) {
		if (is_this_ship(set_of_ships[i], X, Y)) {
			if (set_of_ships[i].getCurDeck() == 0) { // ���� ����� �� ��������, �� ������� ������� � ���������� true 
				//set_of_ships.erase(set_of_ships.begin() + i);
				count_of_ships--;
				return true;
			}
			return false;
		}
	}
	return false;
}

void PlayerBase::add_scores(){
	scores++;
}

void PlayerBase::set_one_ship(int X, int Y, Direction direction, count_of_deck count_of_deck, shared_ptr<Player> player) {
	Ship ship(X, Y, direction, count_of_deck);	// ������� �������
	set_ship_on_map(ship);						// ������������� ������� �� �����
	set_of_ships.push_back(ship);				// ��������� ������� � ��������� 
	gameview->print_board_of_ship(map_ship, player->get_name());	// �������� ��� ������
	count_of_ships++;
}

void PlayerBase::set_ship_on_map(Ship ship) {
	int x1, x2, y1, y2; // ������� ���������� ������� �������
	x1 = ship.getX() - 1;
	y1 = ship.getY() - 1;
	int dX = ship.getX(), dY = ship.getY();
	if (ship.getDir() == h) {
		dY = dY + ship.getDeck() - 1;
		x2 = ship.getX() + 1;
		y2 = ship.getY() + ship.getDeck();
	}
	else {
		dX = dX + ship.getDeck() - 1;
		x2 = ship.getX() + ship.getDeck();
		y2 = ship.getY() + 1;
	}
	// ��������� ������� ���������, ����� ������ 2 ��� �������
	if (x1 < 0) x1++;
	if (y1 < 0) y1++;
	if (x2 > 9) x2--;
	if (y2 > 9) y2--;
	for (int i = x1; i <= x2; i++) {
		for (int j = y1; j <= y2; j++) {
			if (i == N || j == N) continue;
			map_ship[i][j] = 1;
		}
	}

	for (int i = ship.getX(); i <= dX; i++)
		for (int j = ship.getY(); j <= dY; j++)
			map_ship[i][j] = 2;
}
