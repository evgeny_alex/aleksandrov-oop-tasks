#pragma once

enum Direction {h, v};
enum count_of_deck { one = 1, two, three, four };

class Ship {
	private:
		short X, Y;							// ���������� �������
		Direction direction;				// ����������� �������
		count_of_deck count_deck;			// ���������� �����
		int current_count_deck;
	public:
		Ship(short, short, Direction, count_of_deck);	// � ������������ ��������� ����������, ����������� � ���-�� �����
		short getX();
		short getY();
		Direction getDir();
		count_of_deck getDeck();
		void reduce_deck();
		int getCurDeck();
};