#include <iostream>
#include "reader.h"
#include "writer.h"

using namespace std;

int main(int argc, char* argv[]) {
	Reader r(argv[1]);
	Writer w(argv[2], r);
	return 0;
}