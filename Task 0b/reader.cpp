#include <fstream>
#include <string>
#include "reader.h"

using namespace std;

// Конструктор Reader
Reader::Reader(string file_str) {
	ifstream file_in(file_str); // открыли файл для чтения
	Reader::make_map(file_in);
}

/*make_map - функция которая обрабатывает строку из файла, и вставляет каждое слово в
контейнер, увеличивая его частоту. Затем, когда все слова обработались, мы вставляем эти
слова в multimap <int, string>, чтобы слова отсортировались по их частоте.*/

void Reader::make_map(ifstream &file_in) {
	map<string, int> map_1; // храню слово и частоту 
	char c; // отдельный символ
	string word; // буфер для слова	
	map<string, int> ::iterator it; // создали итератор
	int i;

	string str;
	getline(file_in, str, '\0'); // считали файл str

	for (i = 0; str[i] != '\0'; i++) { // проверяем каждый символ
		c = str[i];
		if (check_letter(c)) word += c;
		else {
			if (word == "") continue;
			else insert_into_map(map_1, word, it);// добавляем слово в map
		}
	}
	if ((str[i] == '\0') && (word != "")) // если мы дошли до конца файла и буфер не пустой, то вставляем слово в map
		insert_into_map(map_1, word, it);// добавляем слово в map

	for (it = map_1.begin(); it != map_1.end(); it++) // Вставляем элементы из map в multimap
		Reader::multimap.insert(pair<int, string>(it->second, it->first));
}

/*insert_into_map - функция, которая вставляет слова в map.*/

void Reader::insert_into_map(map<string, int> &map_1, string &word, map<string, int> ::iterator it) {
	it = map_1.find(word); // находим слово
	map_1[word]++; // если оно есть, то увеличиваем частоту
	Reader::count_words++;
	word.clear();
}

/*check_letter - функция, которая проверяет символ с, если с - не разделитель, то добавляем его в буфер*/

int Reader::check_letter(char c) {
	return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

multimap<int, string> Reader::get_multimap() const{
	return Reader::multimap;
}

int Reader::get_count_words() const{
	return Reader::count_words;
}