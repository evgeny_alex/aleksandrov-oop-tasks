#pragma once
#include <string>
#include <map>

using namespace std;

class Reader {
public:
	Reader(string); // Конструктор класса Reader
	multimap<int, string> Reader::get_multimap() const;
	int Reader::get_count_words() const;

	
private:
	void Reader::make_map(ifstream &); // Функция создания контейнера
	void Reader::insert_into_map(map<string, int> &, string &, map<string, int> ::iterator);
	int Reader::check_letter(char);
	int count_words;// Общее количество слов
	multimap<int, string> multimap; // Контейнер для хранения слов и частоты
};
