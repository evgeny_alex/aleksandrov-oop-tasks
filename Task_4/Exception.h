#pragma once
#include <exception>
#include <string>
#include <stdlib.h>

using namespace std;

class Exception : public exception {
protected:
	int line;
	int col;
public:
	int get_line() { return line; }
	int get_col() { return col; }

	virtual const char* what() const throw() = 0;
};

class EmptyValueTuple : public Exception {
public:
	EmptyValueTuple(int l, int c) {
		line = l;
		col = c;
	}
	const char* what() const throw() {
		return "Error! Empty value in tuple.";
	}
};

class NotFoundElementTuple : public Exception {
public:
	NotFoundElementTuple(int l, int c) {
		line = l;
		col = c;
	}
	const char* what() const throw() {
		return "Error! Not found element of tuple.";
	}
};

class InvalidTypeConversion : public Exception {
public:
	InvalidTypeConversion(int l, int c) {
		line = l;
		col = c;
	}
	const char* what() const throw() {
		return "Error! Invalid type conversation.";
	}
};