#include "operator.h"
#include "CSVParser.h"

using namespace std;

int main() {
	ifstream file("C://Users//User//source//repos//Task_4//Debug//test.csv");

	CSVParser<int, double, string> parser(file, 0);

	while (true) {
		try {
			for (tuple<int, double, string> t : parser) {
				cout << t;
				cout << '\n';
			}
			break;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
			cout << "In line: " << e.get_line() << endl;
			cout << "In column: " << e.get_col() << endl;
		}

	}
	system("pause");
	return 0;
}