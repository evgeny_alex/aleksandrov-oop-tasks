#pragma once
#include <iterator>
#include <tuple>
#include <string>
#include <fstream>
#include <iostream>
#include "Exception.h"
#include "iterate_tuple.h"
using namespace std;

template<typename... Args>
class CSVParser {
private:
	int current_line;
	int current_col;
	ifstream& file;
	char sym_str;
	char sym_col;
	char sym_shield; // ������ ������������� ������

	string current_str;
	size_t current_index;

	class write_to_tuple {
	private:
		CSVParser& parser;

		bool check_convert_to_double(string s) {
			for (size_t i = 0; i < s.size(); i++) {
				if (((s[i] > '9') || (s[i] < '0')) && s[i] != '.')
					return true;
			}
			return false;
		}
		bool check_convert_to_int(string s) {
			for (size_t i = 0; i < s.size(); i++) {
				if ((s[i] > '9') || (s[i] < '0'))
					return true;
			}
			return false;
		}

	public:
		write_to_tuple(CSVParser& _parser) : parser(_parser) {}
		
		template<typename T>
		void operator()(int index, T& t) {
			bool read_shield = false;
			string s;
			for (; (this->parser.current_str[this->parser.current_index] != this->parser.sym_col || read_shield); this->parser.current_index++) { // ��������� ������ ������� �������
				if (read_shield) {
					s += this->parser.current_str[this->parser.current_index];
					read_shield = false;
					continue;
				}
				if (this->parser.current_str[this->parser.current_index] == this->parser.sym_shield) {
					read_shield = true;
					continue;
				}
				s += this->parser.current_str[this->parser.current_index];
				}
		}

		template<>
		void operator()(int index, int& t) {
			bool read_shield = false;
			string s;
			if (this->parser.current_index >= this->parser.current_str.size()) throw NotFoundElementTuple(this->parser.current_line, this->parser.current_col);
			for (; ((this->parser.current_str[this->parser.current_index] != this->parser.sym_col || read_shield) && (this->parser.current_str[this->parser.current_index] != '\0')); this->parser.current_index++) { // ��������� ������ ������� �������
				if(this->parser.current_str[this->parser.current_index] == this->parser.sym_str) throw NotFoundElementTuple(this->parser.current_line, this->parser.current_col);
				if (read_shield) {
					s += this->parser.current_str[this->parser.current_index];
					read_shield = false;
					continue;
				}
				if (this->parser.current_str[this->parser.current_index] == this->parser.sym_shield) {
					read_shield = true;
					continue;
				}
				s += this->parser.current_str[this->parser.current_index];
			}
			if (s.size() == 0) throw EmptyValueTuple(this->parser.current_line, this->parser.current_col);
			if (check_convert_to_int(s)) throw InvalidTypeConversion(this->parser.current_line, this->parser.current_col);
			this->parser.current_col++;
			this->parser.current_index++; // ���������� �������������� ������
			t = stoi(s);
		}
		template<>
		void operator()(int index, double& t) {
			bool read_shield = false;
			string s;
			if (this->parser.current_index >= this->parser.current_str.size()) throw NotFoundElementTuple(this->parser.current_line, this->parser.current_col);
			for (; ((this->parser.current_str[this->parser.current_index] != this->parser.sym_col || read_shield) && (this->parser.current_str[this->parser.current_index] != '\0')); this->parser.current_index++) { // ��������� ������ ������� �������
				if (this->parser.current_str[this->parser.current_index] == this->parser.sym_str) throw NotFoundElementTuple(this->parser.current_line, this->parser.current_col);
				if (read_shield) {
					s += this->parser.current_str[this->parser.current_index];
					read_shield = false;
					continue;
				}
				if (this->parser.current_str[this->parser.current_index] == this->parser.sym_shield) {
					read_shield = true;
					continue;
				}
				s += this->parser.current_str[this->parser.current_index];
			}
			if (s.size() == 0) throw EmptyValueTuple(this->parser.current_line, this->parser.current_col);
			if (check_convert_to_double(s)) throw InvalidTypeConversion(this->parser.current_line, this->parser.current_col);
			this->parser.current_col++;
			this->parser.current_index++;
			t = stod(s);
		}
		template<>
		void operator()(int index, string& t) {
			bool read_shield = false;
			string s;
			if (this->parser.current_index >= this->parser.current_str.size()) throw NotFoundElementTuple(this->parser.current_line, this->parser.current_col);
			for (; ((this->parser.current_str[this->parser.current_index] != this->parser.sym_col || read_shield) && (this->parser.current_str[this->parser.current_index] != '\0')); this->parser.current_index++) { // ��������� ������ ������� �������
				if (this->parser.current_str[this->parser.current_index] == this->parser.sym_str) throw NotFoundElementTuple(this->parser.current_line, this->parser.current_col);
				if (read_shield) {
					s += this->parser.current_str[this->parser.current_index];
					read_shield = false;
					continue;
				}
				if (this->parser.current_str[this->parser.current_index] == this->parser.sym_shield) {
					read_shield = true;
					continue;
				}
				s += this->parser.current_str[this->parser.current_index];
			}
			if (s.size() == 0) throw EmptyValueTuple(this->parser.current_line, this->parser.current_col);
			this->parser.current_col++;
			this->parser.current_index++;
			t = s;
		}
	};

	tuple<Args...> read_tuple() {
		current_str = "";
		current_index = 0;
		current_line++;
		current_col = 0;
		tuple<Args...> t;
		getline(file, current_str, sym_str);
		if (current_str.size() == 0) return t;
		For_Each(t, write_to_tuple(*this));
		return t;
	}

	void init_symbols() {
		cout << "Enter symbol of column separator: " << endl;
		cin >> sym_col;
		cout << "Enter symbol of shielding string: " << endl;
		cin >> sym_shield;
	}

	class Iterator {
	private:
		CSVParser& parser;
		tuple<Args...> p;
	public:
		Iterator(tuple<Args...> _p, CSVParser& _parser) : p(_p), parser(_parser) {}

		bool operator!=(Iterator const& other) const {
			return p != other.p;
		}
		bool operator==(Iterator const& other)  const {
			return p == other.p;
		}
		tuple<Args...> operator*() const {
			return p;
		}

		Iterator& operator++() {
			p = this->parser.read_tuple();
			return *this;
		}

	};

public:
	CSVParser(ifstream &file_in, int c) : file(file_in), sym_str('\n'), current_line(c), current_col(1) {
		string str;
		init_symbols();

		for (int i = 0; i < c; i++)
			getline(file, str, sym_str);
	}

	Iterator begin() {
		tuple<Args...> t = read_tuple();
		Iterator it(t, *this);
		return it;
	}
	Iterator end() {
		tuple<Args...> t;
		Iterator it(t, *this);
		return it;
	}
};

