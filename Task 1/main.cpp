#define _CRT_SECURE_NO_WARNINGS
#include "tritset.h"
#include <iostream>
#include <unordered_map>
using namespace std;

int main() {
	TritSet a(100);
	TritSet b(1);

	a[0] = True;
	b[0] = False;

	b[1] = Unknown;
	a[1] = True;

	TritSet c(1);

	c = a | b;
	//a.shrink();
	//cout << a.capacity() << endl;
	//system("pause");
	return 0;
}