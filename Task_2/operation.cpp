#include "operation.h"
#include "exception.h"
#include <string>
#include <stdlib.h>
#include <iostream>

void Oper_Define::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	for (size_t i = 0; i < (list.back()).size(); i++) { // �������� �� ��������, ������� ����� � define
		if ((list.back()[i] == '.') || (list.back()[i] >= '0' && list.back()[i] <= '9')) continue;
		else throw ValException();
	}
	stack_calc.add_to_map(list.front(), stod(list.back())); // � define_map ������ �������� 
}

void Oper_Push::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	double a;
	map<string, double> map = stack_calc.get_map();

	if (map.find(list.front()) != (map.end())) // ���� �������� � �������, �� ���������
		a = (stack_calc.get_map()).at(list.front());
	else {
		for (size_t i = 0; i < (list.back()).size(); i++) { // �������� ���������
			if ((list.back()[i] == '.') || (list.back()[i] == '-') || (list.back()[i] >= '0' && list.back()[i] <= '9')) continue;
			else throw PushStackException();
		}
		a = stod(list.front()); // ���� �����, �� ��������� ������ � double
	}
	stack_calc.push_to_stack(a); // ������ �� ����
}

void Oper_Pop::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	if ((stack_calc.get_stack()).empty()) throw PopStackException();
	stack_calc.pop_from_stack();
}

void Oper_Plus::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	double a, b;
	if (stack_calc.get_stack().empty()) throw ArgStackException();
	a = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	if (stack_calc.get_stack().empty()) {
		stack_calc.push_to_stack(a);
		throw ArgStackException();
	}
	b = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	stack_calc.push_to_stack(a + b);
}

void Oper_Minus::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	double a, b;
	if (stack_calc.get_stack().empty()) throw ArgStackException();
	a = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	if (stack_calc.get_stack().empty()) {
		stack_calc.push_to_stack(a);
		throw ArgStackException();
	}
	b = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	stack_calc.push_to_stack(b - a);
}

void Oper_Mul::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	double a, b;
	if (stack_calc.get_stack().empty()) throw ArgStackException();
	a = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	if (stack_calc.get_stack().empty()) {
		stack_calc.push_to_stack(a);
		throw ArgStackException();
	}
	b = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	stack_calc.push_to_stack(a * b);
}

void Oper_Div::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	double a, b;
	if (stack_calc.get_stack().empty()) throw ArgStackException();
	a = stack_calc.get_stack().top();
	if (a == 0)	throw Div_by_zero_Exception();
	stack_calc.pop_from_stack();
	if (stack_calc.get_stack().empty()) {
		stack_calc.push_to_stack(a);
		throw ArgStackException();
	}
	b = stack_calc.get_stack().top();
	stack_calc.pop_from_stack();
	stack_calc.push_to_stack(b / a);
}

void Oper_Sqrt::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	double a;
	if (stack_calc.get_stack().empty()) throw ArgStackException();
	a = stack_calc.get_stack().top();
	if (a < 0.0) throw Neg_root_Exception();
	stack_calc.pop_from_stack();
	stack_calc.push_to_stack(sqrt(a));
}

void Oper_Print::execute_operation(Stack_Calc &stack_calc, list<string> list) {
	if (stack_calc.get_stack().empty()) throw PrintStackException();
	std::cout << stack_calc.get_stack().top() << endl;
}
