#pragma once
#include <string>
#include <stack>
#include <map>
#include <list>

using namespace std;

class Stack_Calc {
	private:
		stack <double> stack;									//  ���� ������������
		map <string, double> define_map;						//	������� define
		
	public:
		void work(int, char**);							//	������ ������ ������������
		void work_with_console();						//	������ � ��������
		void work_with_file(ifstream&);					//	������ � ������
		void execute_operation(string);					//	���������� ��������
		list <string> read_arg(string, int);			//	���������� ���������� � ����
		void choice_operation(string, list <string>);	//	����� ��������
		double get_top_stack();							//	���������� ������� �����
		std::stack <double> get_stack();
		map <string, double> get_map();
		void add_to_map(string , double);
		void push_to_stack(double);
		void pop_from_stack();
};
