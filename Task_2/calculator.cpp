#include "calculator.h"
#include "operation.h"
#include "exception.h"
#include <iostream>
#include <fstream>
#include <list>

using namespace std;


static map <string, Operation *> create_map();			//	�������� ����������� �������

map<string, Operation*> create_map() {
	map<string, Operation*> operation_map;
	Oper_Def_Creator Creator_Def;
	operation_map["DEFINE"] = Creator_Def.CreateOperation();

	Oper_Print_Creator Creator_Print;
	operation_map["PRINT"] = Creator_Print.CreateOperation();

	Oper_Push_Creator Creator_Push;
	operation_map["PUSH"] = Creator_Push.CreateOperation();
	
	Oper_Pop_Creator Creator_Pop;
	operation_map["POP"] = Creator_Pop.CreateOperation();

	Oper_Plus_Creator Creator_Plus;
	operation_map["+"] = Creator_Plus.CreateOperation();

	Oper_Minus_Creator Creator_Minus;
	operation_map["-"] = Creator_Minus.CreateOperation();

	Oper_Div_Creator Creator_Div;
	operation_map["/"] = Creator_Div.CreateOperation();

	Oper_Mul_Creator Creator_Mul;
	operation_map["*"] = Creator_Mul.CreateOperation();

	Oper_Sqrt_Creator Creator_Sqrt;
	operation_map["SQRT"] = Creator_Sqrt.CreateOperation();
	return operation_map;
}

static const map <string, Operation *> operation_map = create_map();	//	�������, � ������� �������� ��������� �� ��������

void Stack_Calc::work(int argc, char** argv) {	// �������� ������ ������������
	if (argc == 1) work_with_console();
	else {
		ifstream file(argv[1]);
		work_with_file(file);
	}
}

void Stack_Calc::work_with_console() {	// ������ � ��������
	string str;							// ������, ���� ��������� ������� � �� ���������
	do  {
		getline(cin, str);
		if (str == "\0") continue;
		execute_operation(str);			// ���������� ��������

	} while (str.size() != 0);
}

void Stack_Calc::work_with_file(ifstream& file) {	// ������ � ������
	string str;
	do {
		getline(file, str);
		if (str == "\0") continue;
		execute_operation(str);			// ���������� ��������
		
	} while (!file.eof());
}

void Stack_Calc::execute_operation(string str) { // ���������� ��������
	string command;
	int i;
	for (i = 0; ((str[i] != ' ') && (str[i] != '\0')); i++) { // ������� �������
		command += str[i];
	}
	list <string> arguments; // ������ ��� �������� ����������
	if (str[i] == ' ') arguments = read_arg(str, i);
	choice_operation(command, arguments);
}

list<string> Stack_Calc::read_arg(string str, int i) { // ����� ��� ���������� ��������� � ����
	list<string> arguments;
	string arg;
	i++; // ���������� ������
	for (; str[i] != '\n' && str[i] != '\0'; i++) {
		if (str[i] == ' ') { 
			arguments.push_back(arg);
			arg = "";
			continue;
		}
		arg += str[i];
	}
	arguments.push_back(arg);
	return arguments;
}

void Stack_Calc::choice_operation(string command, list <string> arguments) {
	if (command == "#") return;
	if (operation_map.find(command) == operation_map.end()) throw CommandException();
	operation_map.at(command)->execute_operation(*this, arguments);
}

double Stack_Calc::get_top_stack() {
	return stack.top();
}

std::stack<double> Stack_Calc::get_stack() {
	return stack;
}

map<string, double> Stack_Calc::get_map() {
	return define_map;
}

void Stack_Calc::add_to_map(string str, double a) {
	define_map[str] = a;
}

void Stack_Calc::push_to_stack(double a) {
	stack.push(a);
}

void Stack_Calc::pop_from_stack() {
	stack.pop();
}
