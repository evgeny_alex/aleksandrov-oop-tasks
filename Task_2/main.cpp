#include "calculator.h"
#include "exception.h"
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
	Stack_Calc stack_calc;
	while (true) {
		try {
			stack_calc.work(argc, argv);
			return 0;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
	
	return 0;
}