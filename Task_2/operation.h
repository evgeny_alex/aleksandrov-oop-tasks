#pragma once
#include "calculator.h"
#include <stack>
#include <list>
#include <map>

using namespace std;

class Operation {
	public:
		virtual void execute_operation(Stack_Calc &, list<string>) {} // ����� ���������� ��������
};

class Oper_Pop : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Push : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Plus : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Minus : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Mul : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Div : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Sqrt : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Print : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Define : public Operation {
	public:
		void execute_operation(Stack_Calc &, list<string>);
};

class Oper_Creator {
	public:
		virtual Operation* CreateOperation() = 0;
		virtual ~Oper_Creator() {}
};

class Oper_Pop_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Pop;}
};

class Oper_Push_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Push;}
};

class Oper_Plus_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Plus;}
};

class Oper_Minus_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Minus;}
};

class Oper_Mul_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Mul;}
};

class Oper_Div_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Div;}
};

class Oper_Sqrt_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Sqrt;}
};

class Oper_Print_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() {return new Oper_Print;}
};

class Oper_Def_Creator : public Oper_Creator {
	public:
		Operation* CreateOperation() { return new Oper_Define; }
};