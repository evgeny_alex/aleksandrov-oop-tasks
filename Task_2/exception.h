#pragma once
#include <exception>

using namespace std;

class Exception : public exception {
	public:
		virtual const char* what() const throw() = 0;
};

class ValException : public Exception {
	public:
		const char* what() const throw() {
			return "Error! Value in define not double.";
		}
};

class CommandException : public Exception {
	public:
		const char* what() const throw() {
			return "Error! Wrong command.";
		}
};

class StackException : public Exception {
	public:
		virtual const char* what() const throw() = 0;
};

class PushStackException : public StackException {
public:
	const char* what() const throw() {
		return "Error! Push to stack value not double.";
	}
};

class PopStackException : public StackException {
public:
	const char* what() const throw() {
		return "Error! Pop from empty stack.";
	}
};

class ArgStackException : public StackException {
public:
	const char* what() const throw() {
		return "Error! Few arguments.";
	}
};

class PrintStackException : public StackException {
public:
	const char* what() const throw() {
		return "Error! Print value from empty stack.";
	}
};

class MathException : public Exception {
	public:
		const char* what() const throw() {}
};

class Div_by_zero_Exception : public Exception {
	public:
		const char* what() const throw() {
			return "Error! Division by zero.";
		}
};

class Neg_root_Exception : public Exception {
public:
	const char* what() const throw() {
		return "Error! Root from negative number.";
	}
};